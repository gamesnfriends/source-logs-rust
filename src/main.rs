use actix_web::{App, HttpServer};
use dotenv::dotenv;
use sqlx::mysql::MySqlPoolOptions;
use std::env;

mod chatlog;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
    let http_port: u16 = env::var("BIND_PORT")
        .expect("BIND_PORT is not set in .env file")
        .parse::<_>()
        .expect(&format!(
            "BIND_PORT={} must be a number between {}-{}",
            env::var("BIND_PORT").unwrap(),
            u16::MIN,
            u16::MAX
        ));
    let http_addr = env::var("BIND_ADDR").expect("BIND_ADDR is not set in the .env file");
    let db_pool = MySqlPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await
        .unwrap();

    // start http server
    HttpServer::new(move || App::new().data(db_pool.clone()).configure(chatlog::init))
        .bind(http_addr + ":" + http_port.to_string().as_str())?
        .run()
        .await
}
