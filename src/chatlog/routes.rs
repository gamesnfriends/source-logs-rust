use crate::chatlog::Chatlog;
use actix_web::{get, post, web, HttpResponse, Result};
use askama::Template;
use sqlx::MySqlPool;
use std::collections::HashMap;

#[derive(Template)]
#[template(path = "index.html")]
struct Index {
    logs: Vec<Chatlog>,
    total_logs: i64,
    page: u32,
    has_next_page: bool,
    search: Option<String>,
}

static LOGS_PER_PAGE: u32 = 50;

#[get("/")]
async fn index(
    query: web::Query<HashMap<String, String>>,
    db_pool: web::Data<MySqlPool>,
) -> Result<HttpResponse> {
    let page: u32 = match query.get("page") {
        Some(str) => str.parse::<u32>().unwrap_or(0),
        None => 0,
    };
    let start = page * LOGS_PER_PAGE;
    let end = (page + 1) * LOGS_PER_PAGE;
    let chatlogs = match query.get("search") {
        Some(val) => Chatlog::find_search_range(db_pool.get_ref(), start..end, val.clone())
            .await
            .map_err(|_| {
                actix_web::error::ErrorInternalServerError("Database connection failure")
            })?,
        None => Chatlog::find_range(db_pool.get_ref(), start..end)
            .await
            .map_err(|_| {
                actix_web::error::ErrorInternalServerError("Database connection failure")
            })?,
    };
    let has_next_page = chatlogs.logs.len() >= LOGS_PER_PAGE as usize;

    let logs = chatlogs.logs;
    let total_logs = chatlogs.num_logs;

    let s = Index {
        logs,
        total_logs,
        page,
        has_next_page,
        search: match query.get("search") {
            Some(val) => Some(val.clone()),
            None => None,
        },
    }
    .render()
    .map_err(|_| actix_web::error::ErrorInternalServerError("Template generation failure"))?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/")]
async fn search(
    query: web::Query<HashMap<String, String>>,
    db_pool: web::Data<MySqlPool>,
) -> Result<HttpResponse> {
    let page = 0;
    let start = page * LOGS_PER_PAGE;
    let end = (page + 1) * LOGS_PER_PAGE;
    let chatlogs = match query.get("search") {
        Some(val) => Chatlog::find_search_range(db_pool.get_ref(), start..end, val.clone())
            .await
            .map_err(|_| {
                actix_web::error::ErrorInternalServerError("Database connection failure")
            })?,
        None => Chatlog::find_range(db_pool.get_ref(), start..end)
            .await
            .map_err(|_| {
                actix_web::error::ErrorInternalServerError("Database connection failure")
            })?,
    };
    let has_next_page = chatlogs.logs.len() >= LOGS_PER_PAGE as usize;

    let logs = chatlogs.logs;
    let total_logs = chatlogs.num_logs;

    let s = Index {
        logs,
        total_logs,
        page,
        has_next_page,
        search: match query.get("search") {
            Some(val) => Some(val.clone()),
            None => None,
        },
    }
    .render()
    .map_err(|_| actix_web::error::ErrorInternalServerError("Template generation failure"))?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

// function that will be called on new Application to configure routes for this module
pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(index);
    cfg.service(search);
}
