use chrono::NaiveDateTime;
use sqlx::mysql::MySqlPool;
use sqlx::FromRow;
use std::error;
use std::ops::Range;

#[derive(FromRow, Debug)]
pub struct Chatlog {
    pub id: u64,
    pub date: NaiveDateTime,
    pub map: String,
    pub steamid: String,
    pub name: String,
    pub message_style: i8,
    pub message: String,
}

#[derive(Debug)]
pub struct Chatlogs {
    // num_logs is the total number of logs found for the query, not how many were returned (used
    // for example to jump to last page in the web client)
    pub num_logs: i64,
    pub logs: Vec<Chatlog>,
}

impl Chatlog {
    pub async fn find_range(
        pool: &MySqlPool,
        range: Range<u32>,
    ) -> Result<Chatlogs, Box<dyn error::Error>> {
        let mut tx = pool.begin().await?;
        let num_logs = sqlx::query!(
            r#"
                SELECT COUNT(*) as num_logs
                FROM chat_log
            "#,
        )
        .fetch_one(&mut tx);

        let mut tx = pool.begin().await?;
        let recs = sqlx::query_as!(
            Chatlog,
            r#"
                SELECT id, date, map, steamid, name, message_style, message
                FROM chat_log
                ORDER BY date DESC
                LIMIT ?, ?
            "#,
            range.start,
            range.end - range.start
        )
        .fetch_all(&mut tx);

        let num_logs = num_logs.await?.num_logs;
        let logs = recs.await?;

        Ok(Chatlogs { num_logs, logs })
    }

    pub async fn find_search_range(
        pool: &MySqlPool,
        range: Range<u32>,
        search: String,
    ) -> Result<Chatlogs, Box<dyn error::Error>> {
        let mut tx = pool.begin().await?;

        let num_logs = sqlx::query!(
            r#"
                SELECT COUNT(*) as num_logs
                FROM chat_log
                WHERE INSTR(message, ?) > 0 OR INSTR(name, ?) > 0
            "#,
            search,
            search,
        )
        .fetch_one(&mut tx);

        let mut tx = pool.begin().await?;
        let recs = sqlx::query_as!(
            Chatlog,
            r#"
                SELECT id, date, map, steamid, name, message_style, message
                FROM chat_log
                WHERE INSTR(message, ?) > 0 OR INSTR(name, ?) > 0
                ORDER BY date DESC
                LIMIT ?, ?
            "#,
            search,
            search,
            range.start,
            range.end - range.start
        )
        .fetch_all(&mut tx);

        let num_logs = num_logs.await?.num_logs;
        let logs = recs.await?;

        Ok(Chatlogs { num_logs, logs })
    }
}
//
